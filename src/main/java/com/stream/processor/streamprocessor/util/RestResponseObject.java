package com.stream.processor.streamprocessor.util;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

public class RestResponseObject {    
    
    private boolean status;
    private Object payload;
    private String message;

    public RestResponseObject() {
    }
    
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "RestResponseObject{" + "status=" + status + ", payload=" + payload + ", message=" + message + '}';
    }
    
    
}

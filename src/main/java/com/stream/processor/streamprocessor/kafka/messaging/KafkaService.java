package com.stream.processor.streamprocessor.kafka.messaging;

import com.stream.processor.streamprocessor.kafka.factory.KafkaProducerFactory;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@Service
public class KafkaService {

    private static final Logger logger = Logger.getLogger(KafkaService.class.getName());
    @Autowired
    KafkaProducerFactory kafkaProducerFactory;

    public void sendToKafka(String topic, String requestData) {
        //final ProducerRecord<String, String> record = createRecord(requestData);

        ListenableFuture<SendResult<Integer, String>> future = kafkaProducerFactory.kafkaTemplate().send(topic, requestData);
        future.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {

            @Override
            public void onSuccess(SendResult<Integer, String> result) {
                //handleSuccess(data);
                Date date = new Date(result.getRecordMetadata().timestamp());
                Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
                String timestamp = format.format(date);
                logger.log(Level.INFO, "RecordMetadata : - {0}", new Object[]{result.getRecordMetadata().toString()});
                logger.log(Level.INFO, "result: Topic: - {0}  Partition: - {1} Time:-  {2}", new Object[]{result.getRecordMetadata().topic(), result.getRecordMetadata().partition(), timestamp});
            }

            @Override
            public void onFailure(Throwable ex) {
                //handleFailure(data, record, ex);
            }

        });
    }
}

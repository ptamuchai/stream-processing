package com.stream.processor.streamprocessor.model;

import java.util.Date;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@Table("customer")
public class Customer {

    @PrimaryKey
    //private CustomerPrimaryKey userId;
    @PrimaryKeyColumn(name = "email", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private String email;
    private String mobileNumber;
    private String requestId;
    private String cifId;
    private Date dateCreated;
    private Date dateOfBirth;
    private String firstName;
    private String lastName;
    private String gender;
    private String countryCode;
    private String documentType;
    private String identityDocumentNumber;
    private String socialdisplayName;
    private String socialEmail;
    private String socialfirstName;
    private String sociallastName;
    private String socialphotoUrl;
    private String socialPlatform;
    private String referalId;
    private String referedBy;
    private String sourceSystem;
    private String blackListReward;
    private String blackListStar;
    private Date financialcreateDate;
    private Date modifiedDate;
    private String customerrating;
    private String dormant;

    public Customer(String email, String mobileNumber, String firstName, String lastName) {
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getDormant() {
        return dormant;
    }

    public void setDormant(String dormant) {
        this.dormant = dormant;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getCifId() {
        return cifId;
    }

    public void setCifId(String cifId) {
        this.cifId = cifId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getIdentityDocumentNumber() {
        return identityDocumentNumber;
    }

    public void setIdentityDocumentNumber(String identityDocumentNumber) {
        this.identityDocumentNumber = identityDocumentNumber;
    }

    public String getSocialdisplayName() {
        return socialdisplayName;
    }

    public void setSocialdisplayName(String socialdisplayName) {
        this.socialdisplayName = socialdisplayName;
    }

    public String getSocialEmail() {
        return socialEmail;
    }

    public void setSocialEmail(String socialEmail) {
        this.socialEmail = socialEmail;
    }

    public String getSocialfirstName() {
        return socialfirstName;
    }

    public void setSocialfirstName(String socialfirstName) {
        this.socialfirstName = socialfirstName;
    }

    public String getSociallastName() {
        return sociallastName;
    }

    public void setSociallastName(String sociallastName) {
        this.sociallastName = sociallastName;
    }

    public String getSocialphotoUrl() {
        return socialphotoUrl;
    }

    public void setSocialphotoUrl(String socialphotoUrl) {
        this.socialphotoUrl = socialphotoUrl;
    }

    public String getSocialPlatform() {
        return socialPlatform;
    }

    public void setSocialPlatform(String socialPlatform) {
        this.socialPlatform = socialPlatform;
    }

    public String getReferalId() {
        return referalId;
    }

    public void setReferalId(String referalId) {
        this.referalId = referalId;
    }

    public String getReferedBy() {
        return referedBy;
    }

    public void setReferedBy(String referedBy) {
        this.referedBy = referedBy;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getBlackListReward() {
        return blackListReward;
    }

    public void setBlackListReward(String blackListReward) {
        this.blackListReward = blackListReward;
    }

    public String getBlackListStar() {
        return blackListStar;
    }

    public void setBlackListStar(String blackListStar) {
        this.blackListStar = blackListStar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFinancialcreateDate() {
        return financialcreateDate;
    }

    public void setFinancialcreateDate(Date financialcreateDate) {
        this.financialcreateDate = financialcreateDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCustomerrating() {
        return customerrating;
    }

    public void setCustomerrating(String customerrating) {
        this.customerrating = customerrating;
    }

    @Override
    public String toString() {
        return "Customer{"
                + "email='" + email + '\''
                + ", mobileNumber='" + mobileNumber + '\''
                + ", requestId='" + requestId + '\''
                + ", cifId='" + cifId + '\''
                + ", dateCreated=" + dateCreated
                + ", dateOfBirth=" + dateOfBirth
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", gender='" + gender + '\''
                + ", countryCode='" + countryCode + '\''
                + ", documentType='" + documentType + '\''
                + ", identityDocumentNumber='" + identityDocumentNumber + '\''
                + ", socialdisplayName='" + socialdisplayName + '\''
                + ", socialEmail='" + socialEmail + '\''
                + ", socialfirstName='" + socialfirstName + '\''
                + ", sociallastName='" + sociallastName + '\''
                + ", socialphotoUrl='" + socialphotoUrl + '\''
                + ", socialPlatform='" + socialPlatform + '\''
                + ", referalId='" + referalId + '\''
                + ", referedBy='" + referedBy + '\''
                + ", sourceSystem='" + sourceSystem + '\''
                + ", blackListReward='" + blackListReward + '\''
                + ", blackListStar='" + blackListStar + '\''
                + ", financialcreateDate=" + financialcreateDate
                + ", modifiedDate=" + modifiedDate
                + ", customerrating='" + customerrating + '\''
                + ", dormant='" + dormant + '\''
                + '}';
    }
}

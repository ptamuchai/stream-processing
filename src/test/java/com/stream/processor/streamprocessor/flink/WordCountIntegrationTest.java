package com.stream.processor.streamprocessor.flink;

import java.util.List;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.Test;

public class WordCountIntegrationTest {

    private final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

    @Test
    public void givenListOfAmounts_whenUseMapReduce_thenSumAmountsThatAreOnlyAboveThreshold() throws Exception {
        // given
        DataSet<Integer> amounts = env.fromElements(1, 29, 40, 50);
        int threshold = 30;

        // when
        List<Integer> collect = amounts.filter(a -> a > threshold).reduce((integer, t1) -> integer + t1).collect();

        System.out.println("========================= ASSERTION =====================");
        // then
        assertThat(collect.get(0)).isEqualTo(90);
    }

    private static class Person {

        private final int age;
        private final String name;

        private Person(int age, String name) {
            this.age = age;
            this.name = name;
        }
    }
}

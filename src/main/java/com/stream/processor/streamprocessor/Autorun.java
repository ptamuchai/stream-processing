package com.stream.processor.streamprocessor;

import com.stream.processor.streamprocessor.model.Customer;
import com.stream.processor.streamprocessor.repository.CustomerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

//@Component
public class Autorun implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
        clearData();
        saveData();
        lookup();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void clearData() {
        customerRepository.deleteAll();
    }

    public void saveData() {
        System.out.println("===================Save Customers to Cassandra===================");
        Customer cust_1 = new Customer("email1@mail.com", "0763896795", "Peter", "Muchai");
        Customer cust_2 = new Customer("email2@mail.com", "0763896795", "Peter", "Muchai");
        Customer cust_3 = new Customer("email3@mail.com", "0763896795", "Peter", "Muchai");
        Customer cust_4 = new Customer("email4@mail.com", "0763896795", "Peter", "Muchai");
        Customer cust_5 = new Customer("email5@mail.com", "0763896795", "Peter", "Muchai");
        Customer cust_6 = new Customer("email6@mail.com", "0763896795", "Peter", "Muchai");

        // save customers to ElasticSearch
        customerRepository.save(cust_1);
        customerRepository.save(cust_2);
        customerRepository.save(cust_3);
        customerRepository.save(cust_4);
        customerRepository.save(cust_5);
        customerRepository.save(cust_6);
    }

    public void lookup() {
        System.out.println("===================Lookup Customers from Cassandra by Firstname===================");
        List<Customer> peters = customerRepository.findByFirstName("Peter");
        peters.forEach(System.out::println);
    }

}

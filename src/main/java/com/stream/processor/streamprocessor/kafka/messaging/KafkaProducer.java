package com.stream.processor.streamprocessor.kafka.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@Service
public class KafkaProducer {

    private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    //private KafkaTemplate<String, String> kafkaTemplate;
    KafkaService kafkaService;

    @Value("${kafka.topic.customer.query}")
    private String queryCustomer;

    @Value("${kafka.topic.customer.add}")
    private String addCustomer;

    public void send(String data) {
        log.info("sending data='{}'", data);

        kafkaService.sendToKafka(queryCustomer, data);
    }

    public void addCustomer(String data) {
        log.info("sending data='{}'", data);
        kafkaService.sendToKafka(addCustomer, data);
    }
}

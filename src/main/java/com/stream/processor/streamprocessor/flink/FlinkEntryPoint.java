package com.stream.processor.streamprocessor.flink;

import java.util.Properties;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStreamSink;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.util.Collector;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

//@Component
//@Service
public class FlinkEntryPoint {

    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    public void analyzeFromKafka() throws Exception {
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        //properties.setProperty("zookeeper.connect", "localhost:2181");
        properties.setProperty("group.id", "customers");

//        FlinkKafkaConsumer09<String> flinkKafkaConsumer = new FlinkKafkaConsumer09<>("temp", new SimpleStringSchema(),
//                properties);
        
        System.out.println(" ===================== PRINTING FLINK KAFKA STREAMS =================== ");
        
        DataStreamSink<String> stream = env
                .addSource(new FlinkKafkaConsumer09<>("ADD_CUSTOMER", new SimpleStringSchema(), properties))
                .print();

        System.out.println(stream.toString());
        // execute program
        env.execute("Flink Streaming Java API Skeleton");
        
        System.out.println(" ===================== END OF PRINTING FLINK KAFKA STREAMS =================== ");
    }

    public class Splitter implements FlatMapFunction<String, Tuple2<String, Integer>> {

        @Override
        public void flatMap(String sentence, Collector<Tuple2<String, Integer>> out) throws Exception {
            for (String word : sentence.split(" ")) {
                out.collect(new Tuple2<>(word, 1));
            }
        }
    }
}

package com.stream.processor.streamprocessor.kafka.service;

import com.stream.processor.streamprocessor.model.Customer;
import com.stream.processor.streamprocessor.repository.CustomerRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@Service
public class CustomerService {

    private static final Logger log = LoggerFactory.getLogger(CustomerService.class);

    @Autowired
    CustomerRepository customerRepository;

    public void addCustomer(Customer customer) {
        try {
            if (customerRepository.save(customer) != null) {
                log.info("Saved customer " + customer.getEmail());
            } else {
                log.info("Customer save failed " + customer.getEmail());
            }

        } catch (Exception e) {
            log.info("Error " + e.getMessage());
        }
    }

    public List<Customer> findByEmail(String email) {
        List<Customer> customer = new ArrayList<>();
        try {
            customerRepository.findByEmail(email);
        } catch (Exception e) {
        }
        return customer;
    }

}

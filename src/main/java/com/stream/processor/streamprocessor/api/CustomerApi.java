package com.stream.processor.streamprocessor.api;

import com.google.gson.Gson;
import com.stream.processor.streamprocessor.kafka.service.CustomerService;
import com.stream.processor.streamprocessor.kafka.messaging.KafkaProducer;
import com.stream.processor.streamprocessor.model.Customer;
import com.stream.processor.streamprocessor.util.RestResponse;
import com.stream.processor.streamprocessor.util.RestResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@RestController
@RequestMapping("/v1/customer/")
public class CustomerApi {

    @Autowired
    KafkaProducer kafkaProducer;

    @Autowired
    CustomerService customerService;

    @RequestMapping(value = "add", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    public RestResponse addCustomer(@RequestBody Customer customer) {
        RestResponseObject obj = new RestResponseObject();
        Gson g = new Gson();
        String req = g.toJson(customer, Customer.class);
        obj.setMessage("failed");
        obj.setPayload(null);
        obj.setStatus(false);
        if (!customer.getEmail().isEmpty()) {
            if (customerService.findByEmail(customer.getEmail()).isEmpty()) {
                kafkaProducer.addCustomer(req);
                obj.setMessage("success");
                obj.setPayload(customer);
                obj.setStatus(true);
            } else {
                obj.setMessage("customer exists");
                obj.setPayload(null);
                obj.setStatus(false);
            }
        }
        return new RestResponse(obj, HttpStatus.OK);
    }

}

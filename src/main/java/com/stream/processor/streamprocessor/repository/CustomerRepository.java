package com.stream.processor.streamprocessor.repository;

import com.stream.processor.streamprocessor.model.Customer;
import java.util.List;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@Repository
public interface CustomerRepository extends CassandraRepository<Customer, String> {

    @AllowFiltering
    public List<Customer> findByFirstName(String firstname);
    
    @AllowFiltering
    public List<Customer> findByEmail(String email);

}

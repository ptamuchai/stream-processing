package com.stream.processor.streamprocessor;

import java.util.Properties;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@SpringBootApplication
//@EnableAutoConfiguration
public class StreamProcessorApplication {

    public static void main(String[] args) throws Exception {
        
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        
        SpringApplication.run(StreamProcessorApplication.class, args);

        Properties p = new Properties();
        p.setProperty("zookeeper.connect", "localhost:2181");
        p.setProperty("bootstrap.servers", "localhost:9092");
        p.setProperty("group.id", "customers");
        
        FlinkKafkaConsumer09<String> kafkaConsumer = new FlinkKafkaConsumer09<>("ADD_CUSTOMER", new SimpleStringSchema(), p);

        DataStream<String> input = env.addSource(kafkaConsumer);

        input.rebalance().map((String value) -> "Kafka and Flink says: " + value).print();

        env.execute();
    }
}

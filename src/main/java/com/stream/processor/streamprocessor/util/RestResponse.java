package com.stream.processor.streamprocessor.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

public class RestResponse extends ResponseEntity<RestResponseObject>{

    public RestResponse(RestResponseObject t, HttpStatus hs) {
        super(t, hs);
    }
    
}

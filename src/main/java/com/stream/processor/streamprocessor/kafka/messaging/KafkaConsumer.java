package com.stream.processor.streamprocessor.kafka.messaging;

import com.google.gson.Gson;
import com.stream.processor.streamprocessor.kafka.service.CustomerService;
import com.stream.processor.streamprocessor.kafka.storage.KafkaStorage;
import com.stream.processor.streamprocessor.model.Customer;
import java.util.concurrent.CountDownLatch;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@Component
public class KafkaConsumer {

    private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    KafkaStorage storage;
    @Autowired
    CustomerService customerService;

    public CountDownLatch countDownLatch = new CountDownLatch(3);

    public void processMessage(String content) {
        log.info("received content = '{}'", content);
        storage.put(content);
    }

    @KafkaListener(topics = "${kafka.topic.customer.query}")
    public void processMessage(ConsumerRecord<Integer, String> record) {

        //log.info("Acknowledgment : " + acknowledgment.toString());
        log.info("ConsumerRecord : " + record.toString());
        log.info("Listener Id4, Thread ID: " + Thread.currentThread().getId());
        log.info("Received: ******************************** " + record.value() + " " + record.topic() + " partition: " + record.partition());

        countDownLatch.countDown();
    }
    
    @KafkaListener(topics = "${kafka.topic.customer.add}")
    public void addCustomer(ConsumerRecord<Integer, String> record) {

        //log.info("Acknowledgment : " + acknowledgment.toString());
        log.info("ConsumerRecord : " + record.toString());
        log.info("Listener Id4, Thread ID: " + Thread.currentThread().getId());
        log.info("Received: ******************************** " + record.value() + " " + record.topic() + " partition: " + record.partition());
        
        Gson g = new Gson();
        Customer customer = g.fromJson(record.value(), Customer.class);
        customerService.addCustomer(customer);
    }
}

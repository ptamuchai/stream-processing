package com.stream.processor.streamprocessor.api;

import com.stream.processor.streamprocessor.kafka.messaging.KafkaProducer;
import com.stream.processor.streamprocessor.kafka.storage.KafkaStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Muchai Peter <solutions.architect>
 */

@RestController
@RequestMapping(value = "/v1/kafka")
public class WebRestApi {

    @Autowired
    KafkaProducer producer;

    @Autowired
    KafkaStorage storage;

    @GetMapping(value = "/producer")
    public String producer(@RequestParam("data") String data) {
        producer.send(data);

        return "Done";
    }

    @GetMapping(value = "/consumer")
    public String getAllRecievedMessage() {
        String messages = storage.toString();
        storage.clear();

        return messages;
    }

}
